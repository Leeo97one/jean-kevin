package xyz.yolocorp.jeankevin.command;

import com.vdurmont.emoji.EmojiManager;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import org.javacord.api.event.message.MessageCreateEvent;
import xyz.yolocorp.jeankevin.multiple_choice.Question;

import java.util.Arrays;

public class QuestionCommand extends Command {
    public QuestionCommand() {
        name = "question";
        aliases = Arrays.asList("ask", "sondage", "poll");
        description = "Permet de lancer un questionnaire (par exemple : un sondage).";
    }

    @Override
    protected void run(String[] args, MessageCreateEvent event) {
        MessageBuilder message = new MessageBuilder();

        if (args.length < 2) {
            message.setContent("Il faut indiquer au moins une question et une réponse.");
        } else if (args.length > 11) {
            message.setContent("Plus de 10 réponses ? Désolé mais y'a pas moyen.");
        } else {
            Question question = new Question(args[0], event.getMessageAuthor().asUser().get());
            String[] numbers = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "keycap_ten"};

            for (int i = 0; i < args.length - 1; i++) {
                String arg = args[i + 1];

                if (arg.startsWith("-t")) {
                    question.setTime(Integer.decode(arg.substring(3)));
                } else if (arg.startsWith("-u")) {
                    question.setUniqueChoice(Boolean.getBoolean(arg.substring(3)));
                } else {
                    question.addAnswer(arg, EmojiManager.getForAlias(numbers[i]));
                }
            }

            question.sendMessage(event.getChannel());
            return;
        }

        message.send(event.getChannel());
    }

    @Override
    protected MessageBuilder getManual() {
        return new MessageBuilder()
                .append(name, MessageDecoration.BOLD)
                .append(" - ")
                .append(description)
                .appendNewLine()
                .append("Alias : ")
                .append(aliases)
                .appendNewLine()
                .appendNewLine()
                .append("UTILISATION", MessageDecoration.BOLD)
                .appendNewLine()
                .append("\t`!question Question ? | Réponse 1 | Réponse 2 | -t X | ...`")
                .appendNewLine()
                .appendNewLine()
                .append("OPTIONS", MessageDecoration.BOLD)
                .appendNewLine()
                .append("\t`-t X` (optionnel) : termine le questionnaire au bout de X secondes et envoie les résultats.")
                .appendNewLine()
                .appendNewLine();
    }

    @Override
    protected String[] getArgs(String content) {
        return content.substring(super.getArgs(content)[0].length()).split("\\s*\\|\\s*");
    }
}
