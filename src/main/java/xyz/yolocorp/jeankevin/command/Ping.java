package xyz.yolocorp.jeankevin.command;

import org.javacord.api.event.message.MessageCreateEvent;

import java.util.Arrays;
import java.util.Collections;

public class Ping extends Command {
    public Ping() {
        name = "ping";
        aliases = Collections.singletonList("h4ck");
        description = "Ping-pong !";
    }

    @Override
    protected void run(String[] args, MessageCreateEvent event) {
        event.getChannel().sendMessage("Pong !\nargs : " + Arrays.toString(args));
    }
}
