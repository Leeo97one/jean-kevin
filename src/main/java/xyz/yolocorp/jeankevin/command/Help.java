package xyz.yolocorp.jeankevin.command;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.Arrays;

public class Help extends Command {
    public Help() {
        name = "aide";
        aliases = Arrays.asList("aled", "help", "man");
        description = "Affiche la liste des commandes disponibles ou le manuel d'une commande.";
    }

    @Override
    protected void run(String[] args, MessageCreateEvent event) {
        MessageBuilder message = new MessageBuilder();

        if (args.length > 2) {
            message.setContent(args[0] + " n'accepte qu'une seule option au maximum (le nom d'une commande).");
        } else if (args.length == 1) {
            for (Command command : commandSet) {
                message
                        .append(PREFIX)
                        .append(command.getName(), MessageDecoration.BOLD)
                        .append(" : ")
                        .append(command.getDescription())
                        .appendNewLine();
            }
        } else {
            Command command = getCommandByName(args[1]);

            if (command == null) {
                message.setContent("Désolé, je ne reconnais pas la commande « " + args[1] + " ». :thinking:");
            } else {
                message = command.getManual();
            }
        }

        message.send(event.getChannel());
    }
}
