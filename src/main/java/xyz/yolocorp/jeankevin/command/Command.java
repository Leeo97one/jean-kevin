package xyz.yolocorp.jeankevin.command;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import xyz.yolocorp.jeankevin.JeanKevin;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class Command implements MessageCreateListener {
    protected static Set<Command> commandSet = new HashSet<>();

    protected static final String PREFIX = "&";

    protected String name;
    protected List<String> aliases;
    protected String description;

    public static void register(Command command) {
        commandSet.add(command);
        JeanKevin.discordApi.addMessageCreateListener(command);
    }

    public static Command getCommandByName(String name) {
        for (Command command : commandSet) {
            if (command.getName().equals(name) || command.getAliases().contains(name)) {
                return command;
            }
        }

        return null;
    }

    @Override
    public void onMessageCreate(MessageCreateEvent event) {
        String content = event.getMessageContent();
        StringBuilder pattern = new StringBuilder("(" + name);

        for (String alias : aliases) {
            pattern.append("|").append(alias);
        }
        pattern.append(")");

        if (!event.getMessageAuthor().isYourself() &&
                content.matches("^" + PREFIX + pattern + "(\\s+.+)*")) {
            String[] args = getArgs(content);

            run(args, event);
        }
    }

    protected String[] getArgs(String content) {
        return content.split("\\s+");
    }

    protected abstract void run(String[] args, MessageCreateEvent event);

    protected MessageBuilder getManual() {
        return new MessageBuilder().setContent(description);
    }

    public String getName() {
        return name;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public String getDescription() {
        return description;
    }
}
