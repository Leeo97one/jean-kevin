package xyz.yolocorp.jeankevin.multiple_choice;

import com.vdurmont.emoji.Emoji;
import org.javacord.api.entity.user.User;

import java.util.ArrayList;
import java.util.List;

public class Answer {
    private String text;
    private Emoji emoji;
    private List<User> users;

    public Answer(String text, Emoji emoji) {
        this.text = text;
        this.emoji = emoji;
        this.users = new ArrayList<>();
    }

    public String getText() {
        return text;
    }

    public Emoji getEmoji() {
        return emoji;
    }

    public String getTextWithEmoji() {
        return emoji.getUnicode() + " " + text;
    }

    public List<User> getUsers() {
        return users;
    }

    public Answer addUser(User user) {
        users.add(user);
        return this;
    }

    public Answer removeUser(User user) {
        users.remove(user);
        return this;
    }
}
