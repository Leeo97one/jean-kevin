package xyz.yolocorp.jeankevin.multiple_choice;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import org.javacord.api.entity.user.User;

import java.util.*;

public class Question {
    public static Map<Message, Question> questionMap = new HashMap<>();

    private String stem;
    private Map<Emoji, Answer> answerMap;
    private int time;
    private boolean uniqueChoice;
    private User user;
    private MessageBuilder messageBuilder;
    private Message message;

    public Question(String stem, User user) {
        this.stem = stem;
        this.user = user;
        answerMap = new LinkedHashMap<>();
        messageBuilder = new MessageBuilder();
    }

    public Message getMessage() {
        return message;
    }

    public int getTime() {
        return time;
    }

    public Question setTime(int time) {
        this.time = time;
        return this;
    }

    public boolean isUniqueChoice() {
        return uniqueChoice;
    }

    public Question setUniqueChoice(boolean uniqueChoice) {
        this.uniqueChoice = uniqueChoice;
        return this;
    }

    public Answer getAnswerByEmoji(org.javacord.api.entity.emoji.Emoji emoji) {
        return answerMap.get(EmojiManager.getByUnicode(emoji.asUnicodeEmoji().get()));
    }

    public Question addAnswer(String text, Emoji reaction) {
        answerMap.put(reaction, new Answer(text, reaction));
        return this;
    }

    public Message sendMessage(TextChannel channel) {
        buildMessage();
        message = messageBuilder.send(channel).join();
        addReactions();
        questionMap.put(message, this);

        if (time > 0) {
            startTimer();
        }

        return message;
    }

    public Message sendResults() {
        MessageBuilder messageBuilder = new MessageBuilder();

        messageBuilder
                .append("Les questionnaire est terminé, voici les résulats : ")
                .appendNewLine()
                .append(stem, MessageDecoration.BOLD)
                .appendNewLine();

        for (Answer answer : answerMap.values()) {
            messageBuilder
                    .appendNewLine()
                    .append(answer.getTextWithEmoji())
                    .append(" : ")
                    .append(String.valueOf(answer.getUsers().size()), MessageDecoration.BOLD);
        }

        messageBuilder
                .appendNewLine()
                .appendNewLine()
                .append("La question a été proposée par ")
                .append(user.getMentionTag());

        return messageBuilder.send(message.getChannel()).join();
    }

    public void disable() {
        questionMap.remove(message);
    }

    private void buildMessage() {
        messageBuilder.append(stem, MessageDecoration.BOLD).appendNewLine();

        for (Answer answer : answerMap.values()) {
            messageBuilder.appendNewLine().append(answer.getTextWithEmoji());
        }

        messageBuilder.appendNewLine().appendNewLine().append("Question proposée par ").append(user.getMentionTag());
    }

    private void addReactions() {
        for (Emoji emoji : answerMap.keySet()) {
            message.addReaction(emoji.getUnicode());
        }
    }

    public void startTimer() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                disable();
                sendResults();
            }
        }, time * 1000);
    }
}
