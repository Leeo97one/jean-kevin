package xyz.yolocorp.jeankevin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.activity.ActivityType;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.user.User;
import xyz.yolocorp.jeankevin.command.Command;
import xyz.yolocorp.jeankevin.command.Ping;
import xyz.yolocorp.jeankevin.command.Help;
import xyz.yolocorp.jeankevin.command.QuestionCommand;
import xyz.yolocorp.jeankevin.multiple_choice.Answer;
import xyz.yolocorp.jeankevin.multiple_choice.Question;

public class JeanKevin {
    public static DiscordApi discordApi;

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Il manque un argument : le token du bot.");
            System.exit(1);
        }

        final Logger logger = LogManager.getLogger();

        String token = args[0];

        discordApi = new DiscordApiBuilder().setToken(token).login().join();
        logger.info("Le bot est connecté : {}", discordApi.getYourself().getDiscriminatedName());
        logger.info("Lien d'invitation : {}", discordApi.createBotInvite());

        discordApi.updateActivity(ActivityType.LISTENING, "#Yolo");

        discordApi.addServerJoinListener(event -> logger.info("Le bot vient de rejoindre le serveur : {}", event.getServer().getName()));
        discordApi.addServerLeaveListener(event -> logger.info("Le bot vient de quitter le serveur : {}", event.getServer().getName()));

        Command.register(new Ping());
        Command.register(new Help());
        Command.register(new QuestionCommand());

        // Listeners pour ajouter ou supprimer des utilisateurs de la réponse d'une question en fonction de la réaction.
        // TODO: Refactor dans une autre classe.
        discordApi.addReactionAddListener(event -> {
            User user = event.getUser();
            Message message = event.getMessage().get();

            if (Question.questionMap.containsKey(message) && !user.isYourself()) {
                Question question = Question.questionMap.get(message);
                Answer answer = question.getAnswerByEmoji(event.getEmoji());

                answer.addUser(user);
            }
        });
        discordApi.addReactionRemoveListener(event -> {
            User user = event.getUser();
            Message message = event.getMessage().get();

            if (Question.questionMap.containsKey(message) && !user.isYourself()) {
                Question question = Question.questionMap.get(message);
                Answer answer = question.getAnswerByEmoji(event.getEmoji());

                answer.removeUser(user);
            }
        });
    }
}
